## Overarching version 1.0 Goals
Fully-Featured Dev and Testing framework for HTML, CSS, WASM, and Go

 * 0% UI layer (HTML, CSS, animations, id-based updates, 'transactions' of updates)
 * 0% Testing UI layer: Targeting multi-device; multi-platform
 * 0% Progressive Web App
 * 0% UI <-> Model communication layers
 * 0% Model layer (Go - WASM)
 * 0% Client <-> Server communication layers
 * 0% Web Server layer (for non-static data)
 * 0% Hosting server (for static website)
 * 0% Dev Automation: Local Development
 * 0% Dev Automation: CI / CD
 * 0% Docs

## Subgoals and Progress

### UI layer (HTML, CSS, animations, id-based updates, 'transactions' of updates)
Use Hugo to create professional UIs using material.io components
* [ ] Simple HTML and CSS: compile it with Hugo on server & deliver to client
* [ ] Simple HTML and CSS E2E tests
* [ ] Simple animation: compile with Hugo on server & deliver to client
* [ ] Simple animation E2E tests
* [ ] id-based updates: Attach id attributes to HTML & update via Go-WASM func
* [ ] id-based updates: E2E tests
* [ ] Custom components: Text Input with predictions, calculations, validations in UI
* [ ] Custom components: Textarea with predictions, calculations, validation in UI
* [ ] Custom components: Image with predictions, calculations, validation in UI
* [ ] Custom components: Non-Text Input with predictions, calculations, validations in UI
* [ ] Custom components: E2E rendering tests
* [ ] Custom components: E2E accessibility tests
* [ ] Custom components: E2E I18N tests
* [ ] UI components: For each material.io component, allow it to be in UI
* [ ] UI components: E2E tests for rendering each componet
* [ ] UI components: E2E tests for component accessibility (screen reader & contrast)
* [ ] UI components: E2E tests for I18N
* [ ] UI components: Dev doc how to include these in a website
* [ ] UI icons / static assets: For each material.io icon, allow it to be included in UI
* [ ] UI icons / static assets: E2E tests for rendering each Icon
* [ ] UI icons / static assets: E2E tests for accessibility (screen reader & contrast)
* [ ] UI icons / static assets: E2E tests for I18N
* [ ] UI icons / static assets: Dev doc how to include these in a website
* [ ] Create links to all of these Demos on a public web page

### E2E testing: Targeting multi-device; multi-platform
Enable E2E testing for every web page on every type of device
* [ ] E2E testing apparatus for iPhones: small phones
* [ ] E2E testing apparatus for iPhones: large phones
* [ ] E2E testing apparatus for Android: small phones
* [ ] E2E testing apparatus for Android: large phones
* [ ] E2E testing apparatus for iPads
* [ ] E2E testing apparatus for Andoid tablets
* [ ] E2E testing apparatus for half screen web browsers
* [ ] E2E testing apparatus for full screen web browsers
* [ ] E2E testing apparatus for web browsers: Safari
* [ ] E2E testing apparatus for web browsers: Firefox
* [ ] E2E testing apparatus for web browsers: Chrome
* [ ] E2E testing apparatus for web browsers: Edge
* [ ] Run all tests with a single button press - internal only
* [ ] Set up all of this testing apparatus: documentation
* [ ] Set up all of this testing apparatus: One-Click setup

### PWA
* [ ] Create a service worker
* [ ] Run the Lighthouse report as part of the post-deployment E2E test phase
* [ ] Fix any issues given by the Lighthouse report on the test pages

### UI <-> Model communication layers
Allow updating UI with new data and reading existing data
* [ ] Interface for this layer
* [ ] Way to register / instantiate the interface
* [ ] Event: test update
* [ ] Event: Image ID update
* [ ] Event: Audio update
* [ ] Event: Video update
* [ ] Event: Button press update
* [ ] Update text via ID
* [ ] Update images via ID
* [ ] Update audio via ID
* [ ] Update video via ID
* [ ] Transactions of updates: start transaction and commit transaction of updates to UI
* [ ] Transactions of updates: E2E tests
* [ ] Transactions of updates: Make sure UI only re-renders at the end of the transaction

### Model layer (Go - WASM)
* [ ] Interface for the Model Layer
* [ ] Way to register / instantiate the interface
* [ ] Create a data structure which can hold any arbitrary data model
* [ ] Data structure should allow: text, bool, references to files
* [ ] Definitive value (seen in UI)
* [ ] Allow a prediction value - certainty - calculation func data structure
* [ ] Given some data in the data structure, run some code when the data structure changes

### Client <-> Server communication layers
* [ ] Interface for the Communication layer
* [ ] Implementation of the Communication layer
* [ ] From the Model Layer, call the funcs to 'get' the data
* [ ] Communicate for: REST
* [ ] Communicate for: GraphQL
* [ ] Communicate for: gRPC
* [ ] Communicate for: WebHooks
* [ ] From the response, call the funcs to 'put' the data
* [ ] Change the data model: Add a value to the data model via comm.
* [ ] Change the data model: Remove a value from the data model via comm.
* [ ] Debug: See Data Model prior to communication
* [ ] Debug: See Tx Data
* [ ] Debug: See Rx Data
* [ ] Debug: See Data Model after communication

### Web Server layer (for non-static data)
* [ ] Use the same Model Layer Interface we saw on the client code
* [ ] Communicate for: REST
* [ ] Communicate for: GraphQL
* [ ] Communicate for: gRPC
* [ ] Communicate for: WebHooks
* [ ] Run validation, prediction, etc. code which seen in Data Model

### Hosting server (for static website)
* [ ] Go code which can host a static website (serve a directory) over HTTP
* [ ] Go code to serve the static website over HTTPS
* [ ] Provide Authentication and Authorization interface for HTTPS
* [ ] Provide Authentication and Authorization implementation over HTTPS
* [ ] Logging interface you can implement
* [ ] Simple Logging - implement the interface
* [ ] Google Logging - implement the interface
* [ ] Azure Logging - implement the interface
* [ ] AWS Logging - implement the interface
* [ ] Recover from panics & log them - implement the interface
* [ ] Metrics interface: which pages are visited
* [ ] Metrics logging simple - implement the interface
* [ ] Metrics logging Google - implement the interface
* [ ] Metrics logging Azure - implement the interface
* [ ] Metrics logging AWS - implement the interface
* [ ] go get, then go build, then ./xyz -> should run an example website on localhost

### Docs
* [ ] README
* [ ] Godocs
* [x] License

### Dev Automation: Local Development
Quick & Easy local Dev. Quick: <1 second for simple change, Easy: <1 min setup
* [ ] Save text automatically (output: in Editor)
* [ ] Compile HUGO automatically (output: in Editor???)
* [ ] Run Go vet, etc. automatically (output: in Editor)
* [ ] Compile Go automatically (output: binary)
* [ ] Update / refresh webpage automatically (output: in web browser)
* [ ] One-Click Demo
* [ ] Web UI setup: Create a Web UI process to set all of this up automatically

### Dev Automation: CI / CD
Enterprise-grade development. Free, Open Source, CI (Unit Tests, Build),
CD (Publicly Accessible, Scalable, Versioned for Rollback, E2E tests)
* [ ] CI part of the pipeline: Run unit tests
* [ ] CI part of the pipeline: Build
* [ ] CD part of the pipeline: Deploy to Gitlab pages
* [ ] CD part of the pipeline: Deploy to Github pages
* [ ] CD part of the pipeline: Deploy to an arbitrary VM with Go
* [ ] CD part of the pipeline: Deploy to Firebase
* [ ] CD part of the pipeline: Deploy to Azure
* [ ] CD part of the pipeline: Deploy to AWS
* [ ] Rollback for static website: quickly pushing older, working versions
* [ ] CD for Web Server: Deploy to Google App Engine
* [ ] CD for Web Server: Deploy to Kubernetes
* [ ] Rollback for Web Server: Versioning and rollback for GAE
* [ ] Rollback for Web Server: Versioning and rollback fro Kubernetes
* [ ] CD part of the pipeline: Run E2E tests on the deployment @ public URLs
* [ ] CI part of the pipeline: Document how to move changes between environments
* [ ] CI / CD: Document how to create your own Runner for Gitlab for CI/CD
* [ ] CI / CD: Gitlab Runner quickstart: Google VM
* [ ] CI / CD: Gitlab Runner quickstart: Azure VM
* [ ] CI / CD: Gitlab Runner quickstart: AWS VM
* [ ] CI / CD: Using the Runner with Github or another source provider
* [ ] Web UI setup: Create a Web UI process to set all of this up automatically